import java.util.Scanner;
import java.util.Arrays;

public class CalculatorDriver{

	public static void main(String[] args){

		Scanner keyboard = new Scanner(System.in);
		System.out.println("Enter the number of elements you would like to process.");
		int arrayLength = keyboard.nextInt();
		double[] valueArray = new double[arrayLength];

		for(int i = 0; i < valueArray.length; i++){
			System.out.println("Enter element "+(i+1)+".");
			valueArray[i] = keyboard.nextDouble();
			}

		System.out.println("The sum of these "+(valueArray.length + 1)+" elements is "+Calculator.sum(valueArray)+".");
		System.out.println("The average of these "+(valueArray.length + 1)+" elements is "+Calculator.average(valueArray)+".");
		System.out.println("The product of these "+(valueArray.length + 1)+" elements is "+Calculator.product(valueArray)+".");
		}
	}
