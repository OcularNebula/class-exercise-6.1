public class Calculator{

	public static double sum(double[] valueArrayPar){
		double sum = 0;
		for(int i = 0; i < valueArrayPar.length; i++){
			sum += valueArrayPar[i];
			}
		return sum;
		}

	public static double average(double[] valueArrayPar){
		double average = Calculator.sum(valueArrayPar)/valueArrayPar.length;
		return average;
		}

	public static double product(double[] valueArrayPar){
		double product = 1;
		for(int i = 0; i < valueArrayPar.length; i++){
			product *= valueArrayPar[i];
			}
		return product;
		}
		}

